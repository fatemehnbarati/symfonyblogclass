<?php

use Doctrine\ORM\Mapping\ClassMetadataInfo;

$metadata->setInheritanceType(ClassMetadataInfo::INHERITANCE_TYPE_NONE);
$metadata->setChangeTrackingPolicy(ClassMetadataInfo::CHANGETRACKING_DEFERRED_IMPLICIT);
$metadata->mapField(array(
   'fieldName' => 'id',
   'type' => 'integer',
   'id' => true,
   'columnName' => 'id',
  ));
$metadata->mapField(array(
   'columnName' => 'content_ID',
   'fieldName' => 'contentID',
   'type' => 'integer',
  ));
$metadata->mapField(array(
   'columnName' => 'user_ID',
   'fieldName' => 'userID',
   'type' => 'integer',
  ));
$metadata->mapField(array(
   'columnName' => 'context',
   'fieldName' => 'context',
   'type' => 'text',
  ));
$metadata->mapField(array(
   'columnName' => 'timestamp',
   'fieldName' => 'timestamp',
   'type' => 'datetime',
  ));
$metadata->setIdGeneratorType(ClassMetadataInfo::GENERATOR_TYPE_AUTO);