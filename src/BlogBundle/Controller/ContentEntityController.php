<?php

namespace BlogBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use BlogBundle\Entity\ContentEntity;
use BlogBundle\Form\ContentEntityType;

/**
 * ContentEntity controller.
 *
 */
class ContentEntityController extends Controller
{

    /**
     * Lists all ContentEntity entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BlogBundle:ContentEntity')->findAll();

        return $this->render('BlogBundle:ContentEntity:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new ContentEntity entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new ContentEntity();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('contents_show', array('id' => $entity->getId())));
        }

        return $this->render('BlogBundle:ContentEntity:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a ContentEntity entity.
     *
     * @param ContentEntity $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(ContentEntity $entity)
    {
        $form = $this->createForm(new ContentEntityType(), $entity, array(
            'action' => $this->generateUrl('contents_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new ContentEntity entity.
     *
     */
    public function newAction()
    {
        $entity = new ContentEntity();
        $form   = $this->createCreateForm($entity);

        return $this->render('BlogBundle:ContentEntity:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a ContentEntity entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BlogBundle:ContentEntity')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ContentEntity entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BlogBundle:ContentEntity:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing ContentEntity entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BlogBundle:ContentEntity')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ContentEntity entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BlogBundle:ContentEntity:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a ContentEntity entity.
    *
    * @param ContentEntity $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(ContentEntity $entity)
    {
        $form = $this->createForm(new ContentEntityType(), $entity, array(
            'action' => $this->generateUrl('contents_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing ContentEntity entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BlogBundle:ContentEntity')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ContentEntity entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('contents_edit', array('id' => $id)));
        }

        return $this->render('BlogBundle:ContentEntity:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a ContentEntity entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BlogBundle:ContentEntity')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ContentEntity entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('contents'));
    }

    /**
     * Creates a form to delete a ContentEntity entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('contents_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
