<?php

namespace BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ContentEntity
 */
class ContentEntity
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $contentID;

    /**
     * @var integer
     */
    private $userID;

    /**
     * @var string
     */
    private $context;

    /**
     * @var \DateTime
     */
    private $timestamp;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contentID
     *
     * @param integer $contentID
     * @return ContentEntity
     */
    public function setContentID($contentID)
    {
        $this->contentID = $contentID;

        return $this;
    }

    /**
     * Get contentID
     *
     * @return integer 
     */
    public function getContentID()
    {
        return $this->contentID;
    }

    /**
     * Set userID
     *
     * @param integer $userID
     * @return ContentEntity
     */
    public function setUserID($userID)
    {
        $this->userID = $userID;

        return $this;
    }

    /**
     * Get userID
     *
     * @return integer 
     */
    public function getUserID()
    {
        return $this->userID;
    }

    /**
     * Set context
     *
     * @param string $context
     * @return ContentEntity
     */
    public function setContext($context)
    {
        $this->context = $context;

        return $this;
    }

    /**
     * Get context
     *
     * @return string 
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * Set timestamp
     *
     * @param \DateTime $timestamp
     * @return ContentEntity
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    /**
     * Get timestamp
     *
     * @return \DateTime 
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }
}
