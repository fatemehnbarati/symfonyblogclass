<?php

/* BlogBundle:ContentEntity:index.html.twig */
class __TwigTemplate_71c3eff82feec1b73ec53db61764b40b7f344d80fae7a858a88b4cb89b902ff3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "BlogBundle:ContentEntity:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b4761ef04eb1bf4bdb3784a2b5a6ef185ea8028b2a05e229db421fd2dc4200fa = $this->env->getExtension("native_profiler");
        $__internal_b4761ef04eb1bf4bdb3784a2b5a6ef185ea8028b2a05e229db421fd2dc4200fa->enter($__internal_b4761ef04eb1bf4bdb3784a2b5a6ef185ea8028b2a05e229db421fd2dc4200fa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "BlogBundle:ContentEntity:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b4761ef04eb1bf4bdb3784a2b5a6ef185ea8028b2a05e229db421fd2dc4200fa->leave($__internal_b4761ef04eb1bf4bdb3784a2b5a6ef185ea8028b2a05e229db421fd2dc4200fa_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_8b83600279341de495aee1e2ab1ef90998d4ba09f10e7e734396a147ce1f95e1 = $this->env->getExtension("native_profiler");
        $__internal_8b83600279341de495aee1e2ab1ef90998d4ba09f10e7e734396a147ce1f95e1->enter($__internal_8b83600279341de495aee1e2ab1ef90998d4ba09f10e7e734396a147ce1f95e1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<h1>لیست محتوا</h1>

    <table class=\"records_list\" style=\"direction:rtl;\">
        <thead>
            <tr>
                <th>ردیف</th>
                <th>شماره محتوا</th>
                <th>شماره کاربر</th>
                <th>محتوا</th>
                <th>تاریخ ثبت</th>
                <th>عملیات</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 18
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 19
            echo "            <tr>
                <td><a href=\"";
            // line 20
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("contents_show", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
            echo "</a></td>
                <td>";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "contentID", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "userID", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "context", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 24
            if ($this->getAttribute($context["entity"], "timestamp", array())) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entity"], "timestamp", array()), "Y-m-d H:i:s"), "html", null, true);
            }
            echo "</td>
                <td>
                <ul>
                    <li>
                        <a href=\"";
            // line 28
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("contents_show", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
            echo "\">مشاهده</a>
                    </li>
                    <li>
                        <a href=\"";
            // line 31
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("contents_edit", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
            echo "\">اصلاح</a>
                    </li>
                </ul>
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 37
        echo "        </tbody>
    </table>

        <ul>
        <li>
            <a href=\"";
        // line 42
        echo $this->env->getExtension('routing')->getPath("contents_new");
        echo "\">
                Create a new entry
            </a>
        </li>
    </ul>
    ";
        
        $__internal_8b83600279341de495aee1e2ab1ef90998d4ba09f10e7e734396a147ce1f95e1->leave($__internal_8b83600279341de495aee1e2ab1ef90998d4ba09f10e7e734396a147ce1f95e1_prof);

    }

    public function getTemplateName()
    {
        return "BlogBundle:ContentEntity:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  115 => 42,  108 => 37,  96 => 31,  90 => 28,  81 => 24,  77 => 23,  73 => 22,  69 => 21,  63 => 20,  60 => 19,  56 => 18,  40 => 4,  34 => 3,  11 => 1,);
    }
}
