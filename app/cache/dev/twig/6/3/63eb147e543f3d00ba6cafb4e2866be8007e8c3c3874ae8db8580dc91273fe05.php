<?php

/* ::base.html.twig */
class __TwigTemplate_63eb147e543f3d00ba6cafb4e2866be8007e8c3c3874ae8db8580dc91273fe05 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b1ec5ed30b5456ebf87dca8fe749f013441a6e1b728b00660de2d92c07a0f19e = $this->env->getExtension("native_profiler");
        $__internal_b1ec5ed30b5456ebf87dca8fe749f013441a6e1b728b00660de2d92c07a0f19e->enter($__internal_b1ec5ed30b5456ebf87dca8fe749f013441a6e1b728b00660de2d92c07a0f19e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_b1ec5ed30b5456ebf87dca8fe749f013441a6e1b728b00660de2d92c07a0f19e->leave($__internal_b1ec5ed30b5456ebf87dca8fe749f013441a6e1b728b00660de2d92c07a0f19e_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_f514a6af562ed4cf8eb136a0d45b2ba6796128dc91ae1f6020744657908265f2 = $this->env->getExtension("native_profiler");
        $__internal_f514a6af562ed4cf8eb136a0d45b2ba6796128dc91ae1f6020744657908265f2->enter($__internal_f514a6af562ed4cf8eb136a0d45b2ba6796128dc91ae1f6020744657908265f2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_f514a6af562ed4cf8eb136a0d45b2ba6796128dc91ae1f6020744657908265f2->leave($__internal_f514a6af562ed4cf8eb136a0d45b2ba6796128dc91ae1f6020744657908265f2_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_ed0a7174ea0fdd3bb1a0b01b6f51d7e288a8cbd5ed141323eb1c26c8fc378b98 = $this->env->getExtension("native_profiler");
        $__internal_ed0a7174ea0fdd3bb1a0b01b6f51d7e288a8cbd5ed141323eb1c26c8fc378b98->enter($__internal_ed0a7174ea0fdd3bb1a0b01b6f51d7e288a8cbd5ed141323eb1c26c8fc378b98_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_ed0a7174ea0fdd3bb1a0b01b6f51d7e288a8cbd5ed141323eb1c26c8fc378b98->leave($__internal_ed0a7174ea0fdd3bb1a0b01b6f51d7e288a8cbd5ed141323eb1c26c8fc378b98_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_670a0f5b830191fbb1893c432dd76dc0bd129890b7b1624bb45f8b567462a77f = $this->env->getExtension("native_profiler");
        $__internal_670a0f5b830191fbb1893c432dd76dc0bd129890b7b1624bb45f8b567462a77f->enter($__internal_670a0f5b830191fbb1893c432dd76dc0bd129890b7b1624bb45f8b567462a77f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_670a0f5b830191fbb1893c432dd76dc0bd129890b7b1624bb45f8b567462a77f->leave($__internal_670a0f5b830191fbb1893c432dd76dc0bd129890b7b1624bb45f8b567462a77f_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_9f253855330e29bafb2d2a7d4822d43451e87f53891a1500a7e875f2648ba6c8 = $this->env->getExtension("native_profiler");
        $__internal_9f253855330e29bafb2d2a7d4822d43451e87f53891a1500a7e875f2648ba6c8->enter($__internal_9f253855330e29bafb2d2a7d4822d43451e87f53891a1500a7e875f2648ba6c8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_9f253855330e29bafb2d2a7d4822d43451e87f53891a1500a7e875f2648ba6c8->leave($__internal_9f253855330e29bafb2d2a7d4822d43451e87f53891a1500a7e875f2648ba6c8_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 11,  82 => 10,  71 => 6,  59 => 5,  50 => 12,  47 => 11,  45 => 10,  38 => 7,  36 => 6,  32 => 5,  26 => 1,);
    }
}
