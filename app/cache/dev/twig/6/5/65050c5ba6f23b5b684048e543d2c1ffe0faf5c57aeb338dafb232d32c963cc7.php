<?php

/* BlogBundle:ContentEntity:new.html.twig */
class __TwigTemplate_65050c5ba6f23b5b684048e543d2c1ffe0faf5c57aeb338dafb232d32c963cc7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "BlogBundle:ContentEntity:new.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3349bd0f0403610cf1b93a1cccf4b123cd5729b0f43af7d814c7b388731e3c81 = $this->env->getExtension("native_profiler");
        $__internal_3349bd0f0403610cf1b93a1cccf4b123cd5729b0f43af7d814c7b388731e3c81->enter($__internal_3349bd0f0403610cf1b93a1cccf4b123cd5729b0f43af7d814c7b388731e3c81_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "BlogBundle:ContentEntity:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3349bd0f0403610cf1b93a1cccf4b123cd5729b0f43af7d814c7b388731e3c81->leave($__internal_3349bd0f0403610cf1b93a1cccf4b123cd5729b0f43af7d814c7b388731e3c81_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_397803d531be4497aec9700d77dae550ea08b04c3b7e7fb60958d9c873e7a4ab = $this->env->getExtension("native_profiler");
        $__internal_397803d531be4497aec9700d77dae550ea08b04c3b7e7fb60958d9c873e7a4ab->enter($__internal_397803d531be4497aec9700d77dae550ea08b04c3b7e7fb60958d9c873e7a4ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<h1>ContentEntity creation</h1>

    ";
        // line 6
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form');
        echo "

        <ul class=\"record_actions\">
    <li>
        <a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("contents");
        echo "\">
            Back to the list
        </a>
    </li>
</ul>
";
        
        $__internal_397803d531be4497aec9700d77dae550ea08b04c3b7e7fb60958d9c873e7a4ab->leave($__internal_397803d531be4497aec9700d77dae550ea08b04c3b7e7fb60958d9c873e7a4ab_prof);

    }

    public function getTemplateName()
    {
        return "BlogBundle:ContentEntity:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 10,  44 => 6,  40 => 4,  34 => 3,  11 => 1,);
    }
}
